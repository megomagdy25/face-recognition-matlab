%% Recognition(TestImage, m, A, Eigenfaces): compares two faces by projecting the images into facespace and measuring the minimum Euclidean distance between them.
function outputName = Recognition(TestImage, m, A, Eigenfaces)

% Inputs:      TestImage                - Path of the input test image
%
%                m                      - (M*Nx1) Mean of the training
%                                         database, output of 'EigenfaceCore' function.
%
%                Eigenfaces             - (M*Nx(P-1)) Eigen vectors of the
%                                         covariance matrix of the training
%                                         database, output of 'EigenfaceCore' function.
%
%                A                      - (M*NxP) Matrix of centered image
%                                         vectors, output of 'EigenfaceCore' function.
% 
% Returns:       outputName             - Name of the recognized image in the training database.
%

    %% Projecting centered image vectors into facespace
    % All centered images are projected into facespace.
    projectedImages = [];
    trainNumber = size(Eigenfaces, 2);
    
    for i = 1 : trainNumber
        temp = Eigenfaces' * A(:,i);  %Projection of centered images into facespace
        projectedImages = [projectedImages temp]; 
    end

    %% Extracting the PCA features from test image
    inputImage = imread(TestImage);
    %imshow(InputImage)
    temp = inputImage(:,:,1);

    [irow icol] = size(temp);
    InImage = reshape(temp', irow*icol, 1);
    Difference = double(InImage) - m;     % Centered test image
    projectedTestImage = Eigenfaces' * Difference; % Test image feature vector

    %% Calculating Euclidean distances 
    % Euclidean distances between the projected test image and the projection
    % of all centered training images are calculated. 
    % Test image must have minimum distance with its corresponding image in the
    % training database.

    eucDist = [];
    for i = 1 : trainNumber
        q = projectedImages(:,i);
        temp = ( norm (projectedTestImage - q ) )^2;
        eucDist = [eucDist temp];
    end

    [eucDistMin , recognizedIndex] = min(eucDist);
    outputName = strcat(int2str (recognizedIndex), '.jpg');

    %don't forget to change path
    selectedImage = strcat('C:\Users\Mego\Desktop\TrainDatabase\', outputName);
    selectedImage = imread(selectedImage);

    subplot(1, 2, 1), imshow(inputImage);
    title('Tested Image');

    subplot(1, 2, 2), imshow(selectedImage);
    title('Equivalent Image');
    
    str = strcat('Matched image is :  ',outputName);
    disp(str)

end