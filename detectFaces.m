%% FACE DETECTION using Viola Jones Algorithm
%Description: detect faces, crop detected faces, resize to dimension
%162*193 and convert to gray scale images.

%Inputs:    imagesPath: path to original (RGB) images.

%To run: 
%   detectFaces('C:\Users\Mego\Desktop\trainimages\')
%   detectFaces('C:\Users\Mego\Desktop\testimages\')


%% detectFaces(imagesPath): reads RGB images path 
function detectFaces(imagesPath)
    %path to original (RGB) images 
    files = dir(imagesPath);
    NF = length(files);         %length of images in imagesPath
    
    fprintf('Starting ... Running');        %starting message
    
    %j: starts from 3 not 1 as NF has the 2 first structs with no value for name (try to test it indivadually as command to see the result)
    for j = 3:NF
        image = imread(fullfile(imagesPath, files(j).name));    %reading images from directory
        checkImagesPath(imagesPath, j-2);
    end

    %% checkImagesPath(imagesPath, j): function checks images path if "trainimages" or "testimages"
    function checkImagesPath(imagesPath, j)
        %strcmp: compares imagesPath with 'C:\Users\Mego\Desktop\testimages\'
        if(strcmp(imagesPath, 'C:\Users\Mego\Desktop\testimages\'))     
            %output folder to write cropped, face detected then gray scale images in it.
            pathName = 'C:\Users\Mego\Desktop\TestDatabase\';
    
            %check existance of "Output" folder in the specified pathName
            if(~exist(pathName))
                mkdir(pathName);        %create folder "Output" if not found in pathName
            else
                fprintf('\n Output folder is already found! ... Running');
            end
    
        cropResizeImages(pathName, image, j);         %call cropResizeImages function
        
        %strcmp: compares imagesPath with 'C:\Users\Mego\Desktop\trainimages\'
        else if(strcmp(imagesPath, 'C:\Users\Mego\Desktop\trainimages\'))
            pathName = 'C:\Users\Mego\Desktop\TrainDatabase\';
    
            %check existance of "Output" folder in the specified pathName
            if(~exist(pathName))
                mkdir(pathName);        %create folder "Output" if not found in pathName
            else
                fprintf('\n Output folder is already found! ... Running');
            end
            
            cropResizeImages(pathName, image, j);         %call cropResizeImages function
        else
            fprintf('\nCheck: your input folder must be with name: testimages or trainimages');      %failure message
        end  
    end   
end  %end of function checkImagesPath()

    %% cropResizeImages(pathName, image, j): function detect faces, cropes detected faces from images, resizes and converts to gray scale image
    function cropResizeImages(pathName, image, j)
        %figure(1), imshow(image);     %show original image

        %creates imageect to detect faces by deafult
        faceDetect = vision.CascadeObjectDetector; 
        BB = step(faceDetect, image);      %detect face in "image"
        %figure(2), imshow(image);         %show detected face in "image"

        %check number of detected faces in "image"
        for i = 1:size(BB, 1)
            %rectangle('Position',BB(i,:),'LineWidth',3,'LineStyle','-','EdgeColor','r');   %draw red rectangle on the detected face
            imgCropped = imcrop(image, BB(i,:));   %crop image using with the detected face
            imgCroppedResized = imresize(imgCropped, [193 162]);     %rsize to new dimensions 162*193
            imgCroppedResizedGray = rgb2gray(imgCroppedResized);       %convert to gray scale
            %figure(3), subplot(2,2,i); imshow(imgCroppedResizedGray);
            
            % check if there are more than 1 face in the same "image"
            if(i > 1)   
                %renaming images
                outputImageName = sprintf('%s.jpg', strcat(int2str(j), '.', int2str(i)));     %output 'j.i.jpg' where i&j are int
                imwrite(imgCroppedResizedGray, [pathName, outputImageName]);     %write the output image to the pathName
            else
                %renaming images
                outputImageName = sprintf('%d.jpg', j);     %output image with name: 'j.jpg' where j is int
                imwrite(imgCroppedResizedGray, [pathName, outputImageName]);     %write the output image to the pathName
            end  
        end   %end of for

    end    %end of cropResizeImages()

    fprintf('\nFinished ...');      %success message
    
end     %end of detectFaces()


